package com.itheima.dto;

import com.itheima.entity.Dish;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author HeiMa-sw
 * @Description //TODO 17210
 * @Date 9:17 2022/6/8
 **/
@Data
public class DishDto extends Dish implements Serializable{
    private Integer type;
    private String categoryName;
}
