package com.itheima.dto;

import com.itheima.entity.Setmeal;
import com.itheima.entity.SetmealDish;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 套餐数据对象//TODO 17210
 * @Date 16:56 2022/6/8
 **/
@Data
public class SetmealDto extends Setmeal implements Serializable {
    Integer type;
    String categoryName;
    List<SetmealDish> setmealDishes;
}
