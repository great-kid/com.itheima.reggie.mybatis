package com.itheima.dto;

import com.itheima.entity.Dish;
import com.itheima.entity.DishFlavor;
import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;
import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 菜品口味数据对象//TODO 17210
 * @Date 13:39 2022/6/8
 **/
@Data
public class DishFlavorDto extends Dish implements Serializable {
    Integer type;
    List<DishFlavor> flavors;
}
