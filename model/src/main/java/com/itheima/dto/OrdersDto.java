package com.itheima.dto;

import com.itheima.entity.OrderDetail;
import com.itheima.entity.Orders;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 订单数据模型对象//TODO 17210
 * @Date 21:15 2022/6/10
 **/
@Data
public class OrdersDto extends Orders implements Serializable {
    List<OrderDetail> orderDetails;
}
