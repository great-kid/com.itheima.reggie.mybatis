package com.itheima.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author 17210
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressBook {
    String id;
    /**
     * 用户id
     */
    String userId;
    /**
     * 收货人
     */
    String consignee;
    /**
     * 性别 0 女 1 男
     */
    String sex;
    /**
     * 手机号
     */
    String phone;
    /**
     * 省级区划编号
     */
    String provinceCode;
    /**
     * 省级名称
     */
    String provinceName;
    /**
     * 市级区划编号
     */
    String cityCode;
    /**
     * 市级名称
     */
    String cityName;
    /**
     * 区级区划编号
     */
    String districtCode;
    /**
     * 区级名称
     */
    String districtName;
    /**
     * 详细地址
     */
    String detail;
    /**
     * 标签
     */
    String label;
    /**
     * 默认 0 否 1是
     */
    Integer isDefault;
    /**
     * 用户id
     */

    private LocalDateTime createTime;

    private LocalDateTime updateTime;
    String createUser;

    String updateUser;
    /**
     * 是否删除
     */
    Integer isDeleted;
}
