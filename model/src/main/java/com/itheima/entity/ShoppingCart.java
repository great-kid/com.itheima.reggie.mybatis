package com.itheima.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Author HeiMa-sw
 * @Description 购物车//TODO 17210
 * @Date 9:07 2022/6/6
 **/
@Data
public class ShoppingCart {

    private String id;

    /**
     * 名称
     */
    private String name;

    /**
     * 图片
     */
    private String image;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 菜品id
     */
    private String dishId;

    /**
     * 套餐id
     */
    private String setmealId;

    /**
     * 口味
     */
    private String dishFlavor;

    /**
     * 数量
     */
    private Integer number;

    /**
     * 金额
     */
    private BigDecimal amount;

    private LocalDateTime createTime;
}

