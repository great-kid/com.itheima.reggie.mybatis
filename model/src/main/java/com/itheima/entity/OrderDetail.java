package com.itheima.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author HeiMa-sw
 * @Description //TODO 17210
 * @Date 11:12 2022/6/5
 **/
@Data
public class OrderDetail {



    private String id;

    /**
     * 名称
     */
    private String name;

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 菜品id
     */
    private String dishId;

    /**
     * 套餐id
     */
    private String setmealId;

    /**
     * 口味
     */
    private String dishFlavor;


    /**
     * 数量
     */
    private Integer number;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 图片
     */
    private String image;
}
