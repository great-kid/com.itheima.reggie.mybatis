package com.itheima.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author HeiMa-sw
 * @Description 菜品管理//TODO 17210
 * @Date 12:27 2022/6/3
 **/
@Data

public class DishFlavor implements Serializable {


    private String id;

    /**菜品id*/
    private String dishId;

    /**口味名称*/
    private String name;

    /**口味数据list*/
    private String value;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private String createUser;

    private String updateUser;

    /**是否删除*/
    private Integer isDeleted;
}
