package com.itheima.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @Author HeiMa-sw
 * @Description 用户信息//TODO 17210
 * @Date 19:49 2022/6/9
 **/
@Data
public class User {


    private String id;

    /**
     * 姓名
     */
    @NotEmpty(message = "用户姓名不能为空")
    private String name;

    /**
     * 手机号
     */
    @NotEmpty(message = "用户电话号码不能为空")
    private String phone;

    /**
     * 性别 0 女 1 男
     */
    private String sex;

    /**
     * 身份证号
     */
    @NotEmpty(message = "用户身份证不能为空")
    private String idNumber;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 状态 0:禁用，1:正常
     */
    private Integer status;


    private String token;
}

