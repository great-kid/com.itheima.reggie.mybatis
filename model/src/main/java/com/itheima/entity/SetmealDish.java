package com.itheima.entity;


import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class SetmealDish implements Serializable {

    private String id;

    /**
     * 套餐id
     */
    private String setmealId;

    /**
     * 菜品id
     */
    private String dishId;


    /**
     * 菜品名称 （冗余字段）
     */
    private String name;

    /**
     * 菜品原价
     */
    private BigDecimal price;

    /**
     * 份数
     */
    private Integer copies;

    /**
     * 排序
     */
    private Integer sort;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private String createUser;

    private String updateUser;

    /**
     * 是否删除
     */
    private Integer isDeleted;


}
