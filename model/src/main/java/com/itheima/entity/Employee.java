package com.itheima.entity;

import com.itheima.anno.IsHandleResp;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

/**
 * @Author HeiMa-sw
 * @Description 员工实体类//TODO 17210
 * @Date 1:44 2022/6/3
 **/
@Data
public class Employee {

    private String id;

    @NotEmpty(message = "用户名不能为空")
    private String username;
    @NotEmpty(message = "用户姓名不能为空")
    private String name;

    private String password;

    @NotEmpty(message = "电话号码不能为空")
    private String phone;

    private String sex;

    /**
     * 身份证号码
     */
    @NotEmpty(message = "用户身份证不能为空")
    private String idNumber;

    private Integer status;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private String createUser;

    private String updateUser;

    private String Token;
}
