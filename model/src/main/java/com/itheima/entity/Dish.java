package com.itheima.entity;
import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Author HeiMa-sw
 * @Description 菜品实体类//TODO 17210
 * @Date 1:42 2022/6/3
 **/
@Data
public class Dish implements Serializable{
    private String id;

    /**
     * 菜品名称
     */
    private String name;

    /**
     * 菜品分类id
     */
    private String categoryId;


    /**
     * 菜品价格
     */
    private BigDecimal price;

    /**
     * 商品码
     */
    private String code;


    /**
     * 图片
     */
    private String image;


    /**
     * 描述信息
     */
    private String description;


    /**
     * 0 停售 1 起售
     */
    private Integer status;


    /**
     * 顺序
     */
    private Integer sort;



    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private String createUser;

    private String updateUser;

    private Integer isDeleted;
}
