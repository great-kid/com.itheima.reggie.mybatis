package com.itheima.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author HeiMa-sw
 * @Description 分类表实体类//TODO 17210
 * @Date 0:28 2022/6/3
 **/

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Category {


    private String id;

    /**
     * 类型 1 菜品分类 2 套餐分类
     */
    private Integer type;


    /**
     * 分类名称
     */
    private String name;


    /**
     * 顺序
     */
    private Integer sort;


    /**
     * 创建时间
     */

    private LocalDateTime createTime;


    /**
     * 更新时间
     */

    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 修改人
     */
    private String updateUser;

}
