package com.itheima.vo;

import com.itheima.dto.DishDto;
import com.itheima.entity.Dish;
import com.itheima.entity.DishFlavor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 菜品数据对象//TODO 17210
 * @Date 8:55 2022/6/8
 **/
@Data
public class DishVo extends Dish{
    private List<DishDto> records;
    private Integer total;
    private Integer size;
    private Integer current;
}
