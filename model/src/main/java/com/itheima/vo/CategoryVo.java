package com.itheima.vo;

import com.itheima.entity.Category;
import com.itheima.entity.Orders;
import lombok.Data;

import java.util.List;
/**
 * @Author HeiMa-sw
 * @Description 分类分页数据对象//TODO 17210
 * @Date 1:58 2022/6/8
 **/
@Data
public class CategoryVo {
    List<Category> records;
    Integer total;
    Integer size;
    Integer current;
    List<Orders> orders;
    boolean optimizeCountSql = true;
    boolean hitCount = false;
    String countId = null;
    String maLimit = null;
    boolean searchCount = true;
    Integer pages;
}
