package com.itheima.vo;

import com.itheima.dto.SetmealDto;
import com.itheima.entity.Setmeal;
import lombok.Data;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description //TODO 17210
 * @Date 20:01 2022/6/8
 **/
@Data
public class SetmealVo {
    List<SetmealDto> records;
    private Integer total;
    private Integer size;
    private Integer current;
}
