package com.itheima.vo;

import com.itheima.entity.Employee;
import lombok.Data;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 员工数据对象//TODO 17210
 * @Date 19:04 2022/6/7
 **/
@Data
public class EmployeeVo {
    private List<Employee> records;
    private Integer total;
    private Integer size;
    private Integer current;
}
