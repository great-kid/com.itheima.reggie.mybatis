package com.itheima.vo;

import com.itheima.entity.OrderDetail;
import com.itheima.entity.Orders;
import lombok.Data;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 订单表分页对象//TODO 17210
 * @Date 14:52 2022/6/9
 **/
@Data
public class OrdersVo {

    List<Orders> records;
    Integer total;
    Integer size;
    Integer current;
}
