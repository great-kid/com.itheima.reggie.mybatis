package com.itheima.vo;

import com.itheima.dto.OrdersDto;
import com.itheima.entity.Orders;
import lombok.Data;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 移动端分页对象//TODO 17210
 * @Date 21:23 2022/6/10
 **/
@Data
public class OrdersMobileVo extends Orders{
    List<OrdersDto> records;
    Integer total;
    Integer size;
    Integer current;
    Integer page;
}
