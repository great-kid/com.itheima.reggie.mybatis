package com.itheima.mapper;

import com.itheima.entity.Dish;
import com.itheima.entity.Setmeal;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 套餐信息持久层//TODO 17210
 * @Date 16:47 2022/6/8
 **/
@Mapper
public interface SetmealMapper {

    /**
     * 插入套餐信息
     *
     * @param setmeal 套餐
     */
    @Insert("insert into setmeal values (#{id},#{categoryId},#{name},#{price},#{status},#{code},#{description},#{image},#{createTime},#{updateTime},#{createUser},#{updateUser},#{isDelete})")
    void insertOne(Setmeal setmeal);

    /**
     * 查询套餐信息总条数
     *
     * @return int
     */
    @Select("select count(*) from setmeal")
    int selectCount();

    /**
     * 模糊查询分页
     *
     * @param page     当前页数
     * @param pageSize 每页显示条数
     * @param name     姓名
     * @return List<Setmeal>
     */
    List<Setmeal> selectPage(Integer page, Integer pageSize, String name);

    /**
     * 通过id删除套餐
     *
     * @param ids 套餐id数组
     */

    void deleteByIds(String[] ids);

    /**
     * 获取套餐状态为起售的对象
     * @param ids 套餐id数组
     * @return  List<Integer>
     */
    List<Integer> batchSelectStatus(String[] ids);


    /**
     * 通过id获取套餐信息
     *
     * @param id 套餐id
     * @return Setmeal
     */

    Setmeal selectById(String id);

    /**
     * 通过id获取套餐信息
     * @param ids 套餐id数组
     * @return List<Setmeal>
     */
    List<Setmeal> selectByIds(String[] ids);

    /**
     * 套餐批量起售/禁售
     * @param setmealList 套餐集合
     */
    void batchUpdateStatus(List<Setmeal> setmealList);
    /**
     * 通过id修改套餐基本信息
     *
     * @param setmeal 套餐
     */
    void updateOne(Setmeal setmeal);

    /**
     * 通过分类id获取套餐列表
     *
     * @param categoryId 分类id
     * @return 套餐列表
     */
    List<Setmeal> selectListByCid(String categoryId);
}
