package com.itheima.mapper;

import com.itheima.entity.DishFlavor;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.awt.datatransfer.DataFlavor;
import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 菜品口味持久层//TODO 17210
 * @Date 13:48 2022/6/8
 **/
@Mapper
public interface DishFlavorMapper {
    /**
     * 插入菜品口味信息
     *
     * @param dishFlavors 口味集合
     */
    void batchInsert(List<DishFlavor> dishFlavors);

    /**
     * 通过dishId获取口味信息
     *
     * @param id 菜品id
     * @return List<DishFlavor>
     */
    @Select("select * from dish_flavor where dish_id = #{id}")
    List<DishFlavor> selectByDishId(String id);

    /**
     * 菜品口味更改
     *
     * @param dishFlavor 菜品口味
     */
    void updateOne(DishFlavor dishFlavor);

    /**
     * 通过菜品id删除口味信息
     *
     * @param dishIds 菜品id数组
     */
    void batchDeleteByDishIds(String[] dishIds);
}
