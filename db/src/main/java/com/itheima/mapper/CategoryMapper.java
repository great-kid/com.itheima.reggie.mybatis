package com.itheima.mapper;

import com.itheima.entity.Category;
import org.apache.ibatis.annotations.*;


import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 分类持久层接口//TODO 17210
 * @Date 0:56 2022/6/8
 **/
@Mapper
public interface CategoryMapper {
    /**
     * 新增分类
     *
     * @param category 分类
     */
    @Insert("insert into category values (#{id},#{type},#{name},#{sort},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void insertOne(Category category);

    /**
     * 通过名字分类对象
     *
     * @param name 分类名
     * @return Category
     */

    Category selectOne(String name);

    /**
     * 获取分类下拉列表
     * @return 分类列表
     */
    List<Category> selectList();
    /**
     * 查询表中有多少条记录
     *
     * @return int
     */
    @Select("select count(*) from category")
    int selectCount();
    /**
     * 查询分页
     *
     * @param page     当前页数
     * @param pageSize 每页显示条数
     * @return List<Employee>
     */
    List<Category> selectPage(Integer page, Integer pageSize);

    /**
     * 通过id删除分类
     */
    @Delete("delete from category where id = #{id}")
    void deleteById(String id);

    /**
     * 通过id查找分类
     *
     * @param id 分类id
     * @return Category
     */

    @Select("select * from category where id = #{id}")
    Category selectById(String id);


    /**
     * 通过id修改分类信息
     *
     * @param category 分类
     */
    void updateOne(Category category);
}
