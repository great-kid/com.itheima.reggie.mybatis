package com.itheima.mapper;

import com.itheima.entity.OrderDetail;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 订单详情数据持久层//TODO 17210
 * @Date 20:04 2022/6/10
 **/
@Mapper
public interface OrderDetailMapper {
    /**
     * 添加订单详情
     * @param orderDetailList 订单集合
     */
    void insert(List<OrderDetail> orderDetailList);

    /**
     * 通过订单id获取订单详情
     * @param orderId 订单id
     * @return 订单详情列表
     */
    List<OrderDetail> getByOrderId(String orderId);
}
