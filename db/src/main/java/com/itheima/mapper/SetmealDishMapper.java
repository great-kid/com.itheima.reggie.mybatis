package com.itheima.mapper;

import com.itheima.dto.SetmealDto;
import com.itheima.entity.Setmeal;
import com.itheima.entity.SetmealDish;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 套餐菜品信息持久层//TODO 17210
 * @Date 17:05 2022/6/8
 **/
@Mapper
public interface SetmealDishMapper {
    /**
     * 插入套餐菜品集合信息
     *
     * @param setmealDishes 套餐菜品集合信息
     */

    void batchInsert(List<SetmealDish> setmealDishes);

    /**
     * 通过套餐id获取套餐菜品信息
     *
     * @param setmealId 套餐id
     * @return SetmealDish
     */
    @Select("select * from setmeal_dish where setmeal_id = #{setmealId}")
    List<SetmealDish> selectById(String setmealId);

    /**
     * 更新套餐菜品信息
     *
     * @param setmealDish 套餐菜品
     */
    void updateOne(SetmealDish setmealDish);

    /**
     * 通过套餐id删除套餐下菜品信息
     * @param setmealId 套餐id
     */
    @Delete("delete from setmeal_dish where setmeal_id = #{setmealId}")
    void deleteBySid(String setmealId);
}
