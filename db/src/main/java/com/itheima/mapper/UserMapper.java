package com.itheima.mapper;

import com.itheima.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper {

    /**
     * 通过手机号获取用户信息
     * @param phone 手机
     * @return 用户
     */
    User selectOne(String phone);
}
