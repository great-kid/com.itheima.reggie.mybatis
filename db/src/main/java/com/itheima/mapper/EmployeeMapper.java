package com.itheima.mapper;

import com.itheima.entity.Employee;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description //TODO 17210
 * @Date 15:00 2022/6/7
 **/
@Mapper
public interface EmployeeMapper {


    /**
     * 根据账户密码获取用户对象
     *
     * @param username 账户
     * @param password 密码
     * @return 用户对象
     */
    Employee selectOne(@Param("username") String username, @Param("password") String password);

    /**
     * 模糊查询分页
     *
     * @param page     当前页数
     * @param pageSize 每页显示条数
     * @param name     姓名
     * @return List<Employee>
     */
    List<Employee> page(Integer page, Integer pageSize, String name);

    /**
     * 查询表中有多少条记录
     *
     * @return int
     */
    @Select("select count(*) from employee")
    int selectCount();

    /**
     * 通过id或者用户名获取员工对象
     * @param condition 条件
     * @return 员工对象
     */

    Employee selectByIdOrName(String condition);

    /**
     * 添加员工
     *
     * @param employee 员工
     */
    @Insert("insert into employee values " +
            "(#{id},#{name},#{username},#{password},#{phone},#{sex},#{idNumber},#{status},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void insertOne(Employee employee);

    /**
     * 更新员工信息
     *
     * @param employee 员工对象
     */
    void updateOne(Employee employee);

    /**
     * 通过员工用户名查找员工
     * @param userName 用户名
     * @return 员工
     */
    Employee selectByUsername(String userName);

}
