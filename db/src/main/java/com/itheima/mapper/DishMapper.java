package com.itheima.mapper;

import com.itheima.entity.Dish;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 菜品列表持久层//TODO 17210
 * @Date 8:46 2022/6/8
 **/
@Mapper
public interface DishMapper {

    /**
     * 菜品下拉列表
     *
     * @param categoryId 分类id
     * @param name       菜品名
     * @return List<Dish>
     */
    List<Dish> selectList(@Param("categoryId") String categoryId, @Param("name") String name);

    /**
     * 模糊查询分页
     *
     * @param page     当前页数
     * @param pageSize 每页显示条数
     * @param name     姓名
     * @return List<Dish>
     */
    List<Dish> selectPage(Integer page, Integer pageSize, String name);

    /**
     * 查询菜品表有多少条数据
     *
     * @return int
     */
    @Select("select count(*) from dish")
    int selectcount();

    /**
     * 根据id或者菜品名查询菜品
     *
     * @param condition 条件参数
     * @return dish
     */
    Dish selectByIdOrName(String condition);


    /**
     * 插入菜品信息
     *
     * @param dish 菜品
     */
    @Insert("insert into dish values (#{id},#{name},#{categoryId},#{price},#{code},#{image},#{description},#{status},#{sort},#{createTime},#{updateTime},#{createUser},#{updateUser},#{isDeleted})")
    void insertOne(Dish dish);

    /**
     * 更新菜品信息
     *
     * @param dish 菜品集合
     */
    void updateOne(Dish dish);

    /**
     * 更新状态
     *
     * @param dishList 菜品集合
     */
    void updateStatus(List<Dish> dishList);

    /**
     * 通过菜品id数组删除菜品信息
     *
     * @param ids 菜品id数组
     */

    void deleteByIds(String[] ids);

    /**
     * 通过菜品id获取id数组
     *
     * @param ids 菜品id数组
     * @return List<Dish>
     */
    List<Dish> getByIds(String[] ids);
}
