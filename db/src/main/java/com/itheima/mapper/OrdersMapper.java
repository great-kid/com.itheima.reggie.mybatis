package com.itheima.mapper;

import com.itheima.entity.Orders;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 订单持久层接口//TODO 17210
 * @Date 14:48 2022/6/9
 **/
@Mapper
public interface OrdersMapper {
    /**
     * 订单分页
     * @param page 当前页数
     * @param pageSize 每页显示条数
     * @param number 订单编号
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return List<Orders>
     */
    List<Orders> selectPage(Integer page, Integer pageSize, String number, String startTime, String endTime);

    /**
     * 订单总是
     * @return int
     */
    @Select("select count(*) from orders")
    int selectCount();

    /**
     * 修改订单信息
     * @param orders 订单
     */
    void updateOne(Orders orders);

    /**
     * 添加订单信息
     * @param orders 订单
     */
    void insertOne(Orders orders);
}
