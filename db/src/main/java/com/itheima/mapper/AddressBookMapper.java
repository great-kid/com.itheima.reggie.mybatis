package com.itheima.mapper;

import com.itheima.entity.AddressBook;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 地址表持久层映射接口//TODO 17210
 * @Date 8:41 2022/6/10
 **/
@Mapper
public interface AddressBookMapper {

    /**
     * 通过用户id获取默认地址
     *
     * @param userId 用户id
     * @return 收货地址
     */
    AddressBook selectDefault(String userId);

    /**
     * 修改用户收获地址信息
     * @param addressBook 收货地址
     */
    void updateOne(AddressBook addressBook);

    /**
     * 通过用户获取收获地址列表
     *
     * @param userId 用户id
     * @return 地址列表
     */
    List<AddressBook> selectList(String userId);

    /**
     * 通过id获取地址信息
     *
     * @param id 地址id
     * @return 收货地址
     */
    AddressBook selectById(String id);

    /**
     * 添加收获地址
     *
     * @param addressBook 收货地址
     */
    void insertOne(AddressBook addressBook);

    /**
     * 通过id删除收货地址
     *
     * @param id 地址id
     */
    @Delete("delete from address_book where id = #{id}")
    int deleteOne(String id);

    /**
     * 批量更新
     * @param addressBooks 地址
     */
    void updateIsDefault(List<AddressBook> addressBooks);
}
