package com.itheima.mapper;

import com.itheima.entity.ShoppingCart;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 购物车持久层//TODO 17210
 * @Date 20:36 2022/6/9
 **/
@Mapper
public interface ShoppingCartMapper {
    /**
     * 添加购物车
     *
     * @param shoppingCart 购物车
     */
    @Insert("insert into shopping_cart values (#{id},#{name},#{image},#{userId},#{dishId},#{setmealId},#{dishFlavor},#{number},#{amount},#{createTime})")
    void insertOne(ShoppingCart shoppingCart);

    /**
     * 获取购物车列表
     *
     * @return List<ShoppingCart>
     */
    List<ShoppingCart> seleteList();

    /**
     * 清空购物车
     */
    @Delete("delete from shopping_cart")
    void deleteAll();

    /**
     * 通过id删除购物车商品
     *
     * @param dishId 商品id
     */
    int deleteById(@Param("dishId") String dishId, @Param("setmealId") String setmealId);

    /**
     * 通过商品名获取对象
     *
     * @param name 商品名称
     * @return 购物车对象
     */
    ShoppingCart seleteByName(String name);

    /**
     * 通过菜品id获取购物车对象
     *
     * @param dishId 菜品id
     * @return 购物车
     */
    ShoppingCart selectByDishId(String dishId);

    /**
     * 通过套餐id获取购物车对象
     *
     * @param setmealId 套餐id
     * @return 购物车
     */
    ShoppingCart selectBysetmealId(String setmealId);

    /**
     * 更新购物车信息
     *
     * @param shoppingCart 购物车
     */
    int updateOne(ShoppingCart shoppingCart);

    /**
     * 购物车总价
     * @param shoppingCartList 购物车列表
     * @return 总价
     */
    int[] selectAmount(List<ShoppingCart> shoppingCartList);

}
