package com.itheima.service;

import com.itheima.dto.DishFlavorDto;
import com.itheima.entity.Dish;
import com.itheima.entity.DishFlavor;
import com.itheima.vo.DishVo;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 菜品业务层接口//TODO 17210
 * @Date 8:48 2022/6/8
 **/
public interface DishService {
    /**
     * 模糊查询分页
     *
     * @param page     当前页数
     * @param pageSize 每页显示条数
     * @param name     姓名
     * @return List<Dish>
     */
    DishVo getPage(Integer page, Integer pageSize, String name);

    /**
     * 添加菜品
     *
     * @param dishFlavorDto 菜品口味数据对象
     */
    void saveOne(DishFlavorDto dishFlavorDto);

    /**
     * 更新菜品信息
     *
     * @param dishFlavorDto 菜品口味数据对象
     */
    void modifyOne(@RequestBody DishFlavorDto dishFlavorDto);

    /**
     * 更改菜品起售/禁售状态
     *
     * @param status  状态
     * @param id id数组
     */
    void modifyStatus(Integer status, String[] id);

    /**
     * 获取菜品集合
     *
     * @param categoryId 分类id
     * @param name       菜品名
     * @return List<Dish>
     */
    List<Dish> getList(String categoryId, String name);

    /**
     * 通过id获取菜品信息
     *
     * @param id 菜品id
     * @return 菜品
     */
    Dish getById(String id);

    /**
     * 通过菜品id删除菜品
     *
     * @param ids 菜品id数组
     */
    void batchRemove(String[] ids);

    /**
     * 获取菜品列表
     *
     * @param categoryId 分类id
     * @param status     状态
     * @return 菜品数据对象集合
     */
    List<DishFlavorDto> getDishList(String categoryId, Integer status);
}
