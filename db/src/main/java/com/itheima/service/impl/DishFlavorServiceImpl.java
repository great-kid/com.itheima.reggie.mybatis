package com.itheima.service.impl;

import com.itheima.entity.DishFlavor;
import com.itheima.mapper.DishFlavorMapper;
import com.itheima.service.DishFlavorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 菜品口味业务层实现类//TODO 17210
 * @Date 9:23 2022/6/12
 **/
@Service
public class DishFlavorServiceImpl implements DishFlavorService {
    @Autowired
    private DishFlavorMapper dishFlavorMapper;

    @Override
    public void batchRemove(final String[] ids) {
        dishFlavorMapper.batchDeleteByDishIds(ids);
    }

    @Override
    public List<DishFlavor> getByDishId(final String dishId) {
        return dishFlavorMapper.selectByDishId(dishId);
    }
}
