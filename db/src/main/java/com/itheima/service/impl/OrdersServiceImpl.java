package com.itheima.service.impl;

import com.itheima.utils.SnowflakeUtils;
import com.itheima.dto.OrdersDto;
import com.itheima.entity.AddressBook;
import com.itheima.entity.OrderDetail;
import com.itheima.entity.Orders;
import com.itheima.entity.ShoppingCart;
import com.itheima.mapper.AddressBookMapper;
import com.itheima.mapper.OrderDetailMapper;
import com.itheima.mapper.OrdersMapper;
import com.itheima.mapper.ShoppingCartMapper;
import com.itheima.service.OrdersService;
import com.itheima.vo.OrdersMobileVo;
import com.itheima.vo.OrdersVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author HeiMa-sw
 * @Description 订单表业务层实现类//TODO 17210
 * @Date 14:50 2022/6/9
 **/
@Service
public class OrdersServiceImpl implements OrdersService {
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private SnowflakeUtils snowflakeUtils;
    @Autowired
    private OrderDetailMapper orderDetailMapper;
    @Autowired
    private ShoppingCartMapper shoppingCartMapper;
    @Autowired
    private AddressBookMapper addressBookMapper;


    @Override
    public OrdersVo getPage(Integer page, Integer pageSize, String number, String startTime, String endTime) {
        OrdersVo ordersVo = new OrdersVo();
        ordersVo.setCurrent(page);
        if (page == 1) {
            page = 0;
        } else {
            page = (page - 1) * pageSize;
        }
        List<Orders> ordersList = ordersMapper.selectPage(page, pageSize, number, startTime, endTime);
        ordersVo.setRecords(ordersList);
        ordersVo.setSize(pageSize);
        ordersVo.setTotal(ordersMapper.selectCount());
        return ordersVo;
    }

    @Override
    public void modifyOne(final Orders orders) {
        ordersMapper.updateOne(orders);
    }

    @Override
    public void saveOne(final Orders orders) {
        AddressBook addressBook = addressBookMapper.selectById(orders.getAddressBookId());
        BeanUtils.copyProperties(addressBook, orders);
        orders.setAddress(addressBook.getDetail());
        orders.setId(String.valueOf(snowflakeUtils.snowflakeId()));
        orders.setNumber(String.valueOf(snowflakeUtils.snowflakeId()));
        orders.setStatus(2);
        orders.setUserName(orders.getConsignee());
        orders.setOrderTime(LocalDateTime.now());
        orders.setCheckoutTime(LocalDateTime.now());
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.seleteList();
        List<OrderDetail> orderDetailList = shoppingCartList.stream().map(shoppingCart -> {
            OrderDetail orderDetail = new OrderDetail();
            BeanUtils.copyProperties(shoppingCart, orderDetail);
            orderDetail.setId(String.valueOf(snowflakeUtils.snowflakeId()));
            orderDetail.setOrderId(orders.getId());
            return orderDetail;
        }).collect(Collectors.toList());
        orderDetailMapper.insert(orderDetailList);
        int[] amount = shoppingCartMapper.selectAmount(shoppingCartList);
        int sum = Arrays.stream(amount).sum();
        orders.setAmount(BigDecimal.valueOf(sum));
        ordersMapper.insertOne(orders);
    }

    @Override
    public OrdersMobileVo getPage(Integer page, final Integer pageSize) {
        OrdersMobileVo ordersMobileVo = new OrdersMobileVo();
        ordersMobileVo.setCurrent(page);
        if (page == 1) {
            page = 0;
        } else {
            page = (page - 1) * pageSize;
        }
        List<Orders> ordersList = ordersMapper.selectPage(page, pageSize, null, null, null);
        List<OrdersDto> ordersDtos = ordersList.stream().map(orders -> {
            OrdersDto ordersDto = new OrdersDto();
            BeanUtils.copyProperties(orders, ordersDto);
            List<OrderDetail> orderDetails = orderDetailMapper.getByOrderId(orders.getId());
            ordersDto.setOrderDetails(orderDetails);
            return ordersDto;
        }).collect(Collectors.toList());
        ordersMobileVo.setRecords(ordersDtos);
        ordersMobileVo.setSize(pageSize);
        int count = ordersMapper.selectCount();
        ordersMobileVo.setTotal(count);
        ordersMobileVo.setPage((int) Math.ceil(count * 1.0 / pageSize));
        return ordersMobileVo;
    }
}
