package com.itheima.service.impl;

import com.itheima.common.AuthThreadLocal;
import com.itheima.utils.SnowflakeUtils;
import com.itheima.entity.AddressBook;
import com.itheima.mapper.AddressBookMapper;
import com.itheima.service.AddressBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 地址表业务层实现类//TODO 17210
 * @Date 8:42 2022/6/10
 **/
@Service
public class AddressBookServiceImpl implements AddressBookService {
    @Autowired
    private AddressBookMapper addressBookMapper;
    @Autowired
    private SnowflakeUtils snowflakeUtils;

    @Override
    public AddressBook saveOne(final AddressBook addressBook) {
        String uid = AuthThreadLocal.getAuth();
        addressBook.setId(String.valueOf(snowflakeUtils.snowflakeId()));
        addressBook.setUserId(uid);
        addressBook.setIsDefault(0);
        addressBook.setCreateUser(uid);
        addressBook.setUpdateUser(uid);
        addressBook.setCreateTime(LocalDateTime.now());
        addressBook.setUpdateTime(LocalDateTime.now());
        addressBook.setIsDeleted(0);
        addressBookMapper.insertOne(addressBook);
        return addressBook;
    }

    @Override
    public AddressBook getDefault(final String userId) {
        return addressBookMapper.selectDefault(userId);
    }

    @Override
    public AddressBook getById(final String id) {
        return addressBookMapper.selectById(id);
    }

    @Override
    public List<AddressBook> getList(final String userId) {
        return addressBookMapper.selectList(userId);
    }

    @Override
    public int removeOne(final String id) {
        return addressBookMapper.deleteOne(id);
    }

    @Override
    public void updateIsDefault(final List<AddressBook> addressBooks) {
        addressBookMapper.updateIsDefault(addressBooks);
    }

    @Override
    public void modifyOne(final AddressBook addressBook) {
        addressBookMapper.updateOne(addressBook);
    }
}
