package com.itheima.service.impl;

import com.itheima.common.AuthThreadLocal;
import com.itheima.common.CustomException;
import com.itheima.utils.SnowflakeUtils;
import com.itheima.entity.Category;
import com.itheima.mapper.CategoryMapper;
import com.itheima.mapper.DishMapper;
import com.itheima.mapper.EmployeeMapper;
import com.itheima.mapper.SetmealMapper;
import com.itheima.service.CategoryService;
import com.itheima.vo.CategoryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 分类业务层接口//TODO 17210
 * @Date 20:26 2022/6/12
 **/
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private SnowflakeUtils snowflakeUtils;

    @Override
    public CategoryVo getPage(Integer page, Integer pageSize) {
        CategoryVo categoryVo = new CategoryVo();
        categoryVo.setCurrent(page);
        if (page == 1) {
            page = 0;
        } else {
            page = (page - 1) * pageSize;
        }
        List<Category> categoryList = categoryMapper.selectPage(page, pageSize);
        categoryVo.setRecords(categoryList);
        categoryVo.setTotal(employeeMapper.selectCount());
        categoryVo.setSize(pageSize);
        return categoryVo;
    }

    @Override
    public void removeOne(final String id) {
        Category category = categoryMapper.selectById(id);
        if (category.getType() == 1) {
            if (dishMapper.selectList(id, null).isEmpty()) {
                categoryMapper.deleteById(id);
            } else {
                throw new CustomException("当前分类下关联了菜品，不能删除");
            }
        } else if (setmealMapper.selectListByCid(id).isEmpty()) {
            categoryMapper.deleteById(id);
        } else {
            throw new CustomException("当前分类下关联了套餐，不能删除");
        }
    }


    @Override
    public void saveOne(final Category category) {
        category.setId(String.valueOf(snowflakeUtils.snowflakeId()));
        category.setCreateTime(LocalDateTime.now());
        category.setUpdateTime(LocalDateTime.now());
        String empId = AuthThreadLocal.getAuth();
        category.setCreateUser(empId);
        category.setUpdateUser(empId);
        Category one = categoryMapper.selectOne(category.getName());
        if (one != null) {
            throw new CustomException("分类名已经存在");
        }
        categoryMapper.insertOne(category);
    }

    @Override
    public List<Category> getList() {
        return categoryMapper.selectList();

    }

    @Override
    public void modifyOne(final Category category) {
        String empId = AuthThreadLocal.getAuth();
        category.setUpdateTime(LocalDateTime.now());
        category.setUpdateUser(empId);
        categoryMapper.updateOne(category);
    }
}


