package com.itheima.service.impl;

import com.itheima.entity.User;
import com.itheima.mapper.UserMapper;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author HeiMa-sw
 * @Description 用户业务层实现类//TODO 17210
 * @Date 17:03 2022/6/9
 **/
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public User getOne(String phone) {
        return userMapper.selectOne(phone);
    }
}
