package com.itheima.service;

import com.itheima.dto.DishFlavorDto;
import com.itheima.dto.SetmealDto;
import com.itheima.vo.SetmealVo;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 套餐业务层接口//TODO 17210
 * @Date 13:42 2022/6/10
 **/
public interface SetmealService {
    /**
     * 添加套餐信息
     *
     * @param setmealDto 套餐数据对象
     */
    void saveOne(SetmealDto setmealDto);

    /**
     * 模糊查询分页
     *
     * @param page     当前页数
     * @param pageSize 每页显示条数
     * @param name     姓名
     * @return List<Dish>
     */
    SetmealVo getPage(Integer page, Integer pageSize, String name);

    /**
     * 通过id获取套餐数据对象信息
     *
     * @param setmealId 套餐id
     * @return 套餐数据对象
     */
    SetmealDto getById(String setmealId);

    /**
     * 删除套餐
     *
     * @param ids 套餐id
     */
    void batchRemove(String[] ids);

    /**
     * 更新套餐信息
     *
     * @param setmealDto 套餐数据对象
     */
    void modifyOne(SetmealDto setmealDto);

    /**
     * 更新套餐起售/禁售
     *
     * @param status 状态
     * @param ids    套餐id数组
     */
    void modifyStatus(Integer status, String[] ids);
    /**
     * 查询套餐列表
     * @param categoryId 分类编号
     * @param status 状态
     * @return 套餐数据对象
     */
    List<SetmealDto> getList(String categoryId, Integer status);

    /**
     * 通过菜品id获取菜品详情
     * @param setmealId 套餐id
     * @return 菜品口味数据对象
     */
    List<DishFlavorDto> getDishFlavorsList(String setmealId);
}
