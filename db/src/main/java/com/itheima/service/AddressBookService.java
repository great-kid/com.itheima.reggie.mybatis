package com.itheima.service;

import com.itheima.entity.AddressBook;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 用户地址表业务层接口//TODO 17210
 * @Date 8:43 2022/6/10
 **/
public interface AddressBookService {

    /**
     * 添加收货信息
     *
     * @param addressBook 收货地址
     * @return 收货地址
     */
    AddressBook saveOne(AddressBook addressBook);

    /**
     * 获取用户的默认地址
     * @param userId 用户id
     * @return 用户地址
     */
    AddressBook getDefault(String userId);

    /**
     * 通过id获取地址
     * @return 地址id
     */
    AddressBook getById(String id);

    /**
     * 获取用户的地址列表
     * @param userId 用户id
     * @return 地址列表
     */
    List<AddressBook>  getList(String userId);

    /**
     * 通过id删除收货地址
     *
     * @param id 地址id
     */
    int removeOne(String id);

    /**
     * 批量更新
     * @param addressBooks 地址
     */
    void updateIsDefault(List<AddressBook> addressBooks);

    /**
     * 修改用户收获地址信息
     * @param addressBook 收货地址
     */
    void modifyOne(AddressBook addressBook);
}
