package com.itheima.service;

import com.itheima.entity.Employee;
import com.itheima.vo.EmployeeVo;

/**
 * @Author HeiMa-sw
 * @Description 员工业务层接口//TODO 17210
 * @Date 15:31 2022/6/7
 **/
public interface EmployeeService {

    /**
     * 员工分页
     *
     * @param page     当前页数
     * @param pageSize 每页显示条数
     * @param name     员工名字
     * @return List<Employee>
     */
    EmployeeVo getPage(Integer page, Integer pageSize, String name);

    /**
     * 添加员工
     *
     * @param employee 员工对象
     * @return Employee
     */
    Employee saveOne(Employee employee);

    /**
     * 更新员工信息
     *
     * @param employee 员工对象
     * @return Employee
     */
    Employee modifyOne(Employee employee);

    /**
     * 获取员工对象
     *
     * @param username 用户名
     * @param password 密码
     * @return 员工对象
     */
    Employee getOne(String username, String password);

    /**
     * 通过id或者用户名获取用户信息
     * @param condition 条件
     * @return 用户
     */
    Employee getByIdOrName(String condition);
}
