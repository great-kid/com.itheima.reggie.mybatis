package com.itheima.service;

import com.itheima.entity.DishFlavor;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 菜品口味业务层接口//TODO 17210
 * @Date 9:23 2022/6/12
 **/
public interface DishFlavorService {
    /**
     * 通过菜品id删除口味信息
     *
     * @param ids 菜品id数组
     */
    void batchRemove(String[] ids);

    /**
     * 通过菜品id获取口味数组
     *
     * @param dishId 菜品id
     * @return 口味数组
     */
    List<DishFlavor> getByDishId(String dishId);
}
