package com.itheima.service;

import com.itheima.entity.User;

/**
 * @Author HeiMa-sw
 * @Description 用户业务层接口//TODO 17210
 * @Date 17:03 2022/6/9
 **/
public interface UserService {
    /**
     * 通过手机号获取用户
     * @param phone
     * @return 用户
     */
    User getOne(String phone);
}
