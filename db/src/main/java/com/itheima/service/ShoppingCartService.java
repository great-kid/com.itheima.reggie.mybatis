package com.itheima.service;

import com.itheima.entity.ShoppingCart;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 购物车业务层接口//TODO 17210
 * @Date 20:58 2022/6/9
 **/
public interface ShoppingCartService {
    /**
     * 添加购物车
     *
     * @param shoppingCart 购物车
     * @return ShoppingCart
     */
    ShoppingCart saveOne(ShoppingCart shoppingCart);

    /**
     * 删除购物车中的商品信息
     *
     * @param dishId    菜品id
     * @param setmealId 套餐id
     */
    int removeOne(String dishId, String setmealId);

    /**
     * 获取购物车列表
     *
     * @return 购物车列表
     */
    List<ShoppingCart> getList();

    /**
     * 清空购物车
     */
    void cleanAll();
}
