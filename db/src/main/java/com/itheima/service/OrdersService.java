package com.itheima.service;

import com.itheima.entity.Orders;
import com.itheima.vo.OrdersMobileVo;
import com.itheima.vo.OrdersVo;

/**
 * @Author HeiMa-sw
 * @Description 订单表业务层接口//TODO 17210
 * @Date 14:50 2022/6/9
 **/
public interface OrdersService {
    /**
     * 订单分页
     *
     * @param page      当前页数
     * @param pageSize  每页显示条数
     * @param number    订单编号
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 订单分页数据
     */
    OrdersVo getPage(Integer page, Integer pageSize, String number, String startTime, String endTime);

    /**
     * 更新订单表
     * @param orders 订单
     */
    void modifyOne(Orders orders);

    /**
     * 提交订单
     *
     * @param orders  订单
     */
    void saveOne(Orders orders);

    /**
     * 订单分页
     * @param page 当前页数
     * @param pageSize 每页显示条数
     * @return OrdersMobileVo
     */
    OrdersMobileVo getPage(Integer page, Integer pageSize);
}
