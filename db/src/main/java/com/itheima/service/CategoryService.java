package com.itheima.service;

import com.itheima.entity.Category;
import com.itheima.vo.CategoryVo;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 分类业务接口//TODO 17210
 * @Date 20:26 2022/6/12
 **/
public interface CategoryService {
    /**
     * 员工分页
     * @param page 当前页数
     * @param pageSize 每页显示条数
     * @return  List<Category>
     */
    CategoryVo getPage(Integer page, Integer pageSize);

    /**
     * 通过id删除分类
     * @param categoryId 分类id
     */
    void removeOne(String categoryId);

    /**
     * 添加分类
     * @param category 分类
     */
    void saveOne(final Category category);

    /**
     * 获取分类列表
     * @return 分类列表
     */
    List<Category> getList();

    /**
     * 更新分类信息
     * @param category 分类
     */
    void modifyOne(Category category);
}
