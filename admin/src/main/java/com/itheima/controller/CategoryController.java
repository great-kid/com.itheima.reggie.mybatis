package com.itheima.controller;

import com.itheima.common.R;
import com.itheima.entity.Category;
import com.itheima.service.CategoryService;
import com.itheima.vo.CategoryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 分类操作控制层//TODO 17210
 * @Date 1:32 2022/6/8
 **/
@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @PostMapping
    public R<String> categorySave(@RequestBody Category category) {
        try {
            categoryService.saveOne(category);
            return R.success("添加成功");
        } catch (Exception e) {
            return R.error(e.getMessage());
        }
    }

    @GetMapping("/list")
    public R<List<Category>> categoryList() {
        List<Category> categories = categoryService.getList();
        return R.success(categories);
    }

    @GetMapping("/page")
    private R<CategoryVo> categoryPage(Integer page, Integer pageSize) {
        CategoryVo pageInfo = categoryService.getPage(page, pageSize);
        return R.success(pageInfo);
    }

    @DeleteMapping
    private R<String> removeCategory(String id) {
        try {
            categoryService.removeOne(id);
            return R.success("删除成功");
        } catch (Exception e) {
            return R.error(e.getMessage());
        }
    }


    @PutMapping
    private R<String> modifyCategory(@RequestBody Category category) {
        categoryService.modifyOne(category);
        return R.success("修改分类信息成功");
    }

}
