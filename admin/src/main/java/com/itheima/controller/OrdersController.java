package com.itheima.controller;

import com.itheima.common.R;
import com.itheima.entity.Orders;
import com.itheima.service.OrdersService;
import com.itheima.vo.OrdersVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author HeiMa-sw
 * @Description 订单表操作//TODO 17210
 * @Date 14:51 2022/6/9
 **/
@RestController
@RequestMapping("/order")
public class OrdersController {
    @Autowired
    private OrdersService ordersService;

    @GetMapping("/page")
    public R<OrdersVo> orderPage(Integer page, Integer pageSize, String number, String beginTime, String endTime) {
        OrdersVo pageInfo = ordersService.getPage(page, pageSize, number, beginTime, endTime);
        return R.success(pageInfo);
    }

    @PutMapping
    public R<String> orderModify(@RequestBody Orders orders) {
        ordersService.modifyOne(orders);
        return R.success("操作成功");
    }
}
