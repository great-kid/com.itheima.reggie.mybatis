package com.itheima.controller;

import com.itheima.anno.IsHandleResp;
import com.itheima.common.CustomException;
import com.itheima.common.R;
import com.itheima.entity.Employee;
import com.itheima.service.EmployeeService;
import com.itheima.utils.JwtUtils;
import com.itheima.vo.EmployeeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author HeiMa-sw
 * @Description 员工表现层//TODO 17210
 * @Date 15:58 2022/6/7
 **/
@RestController
@RequestMapping("/employee")
public class  EmployeeController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @PostMapping("/login")
    @IsHandleResp(params = "name,phone,idNumber", type = ("3,4,2"))
    public R<Employee> employeeLogin(@RequestBody Employee employee) {
        //获取加密后的密码
        String password = DigestUtils.md5DigestAsHex(employee.getPassword().getBytes());
        //获取员工列表遍历对比登录
        Employee emp = employeeService.getOne(employee.getUsername(), password);
        if (emp != null) {
            if (emp.getStatus() == 1) {
                Map<String,Object> map = new HashMap<>();
                map.put("empId",emp.getId());
                String token = JwtUtils.getToken(map);
                emp.setToken(token);
                return R.success(emp);
            } else {
                return R.error("该账号已被禁用");
            }
        } else {
            return R.error("用户名或密码错误");
        }
    }

    @PostMapping("/logout")
    public R<String> employeeLogout() {
        return R.success("退出成功!");
    }

    @GetMapping("/page")
    public R<EmployeeVo> employeePage(Integer page, Integer pageSize, String name) {
        EmployeeVo pageInfo = employeeService.getPage(page, pageSize, name);
        return R.success(pageInfo);
    }

    @GetMapping("/{id}")
    public R<Employee> employeeId(@PathVariable String id) {
        Employee employee = employeeService.getByIdOrName(id);
        return R.success(employee);
    }

    @PostMapping
    @IsHandleResp(params = "name,phone,idNumber", type = ("3,4,2"))
    public R<Employee> saveEmployee(@RequestBody Employee employee) {
        Employee emp = employeeService.getByIdOrName(employee.getUsername());
        if (emp == null) {
            Employee e1 = employeeService.saveOne(employee);
            return R.success(e1);
        } else {
            return R.error(employee.getUsername() + "已存在");
        }
    }

    @PutMapping
    public R<Employee> updateEmployee(@RequestBody Employee employee) {
        try {
            Employee e1 = employeeService.modifyOne(employee);
            return R.success(e1);
        } catch (CustomException e) {
            return R.error(e.getMessage());
        }
    }
}

