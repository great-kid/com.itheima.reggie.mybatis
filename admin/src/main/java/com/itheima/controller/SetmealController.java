package com.itheima.controller;

import com.itheima.common.CustomException;
import com.itheima.common.R;
import com.itheima.dto.SetmealDto;
import com.itheima.service.SetmealService;
import com.itheima.vo.SetmealVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

/**
 * @Author HeiMa-sw
 * @Description 套餐操作层//TODO 17210
 * @Date 18:42 2022/6/8
 **/
@RestController
@RequestMapping("/setmeal")
public class SetmealController {
    @Autowired
    private SetmealService setmealService;

    @PostMapping
    @CacheEvict(value = "setMeal", allEntries = true)
    public R<SetmealDto> saveSetmeal(@RequestBody SetmealDto setmealDto) {
        setmealService.saveOne(setmealDto);
        return R.success(setmealDto);
    }


    @GetMapping("/page")
    public R<SetmealVo> setmealPage(Integer page, Integer pageSize, String name) {
        SetmealVo pageInfo = setmealService.getPage(page, pageSize, name);
        return R.success(pageInfo);
    }

    @DeleteMapping
    @CacheEvict(value = "setMeal", allEntries = true)
    public R<String> batchRemoveSetmeal(@RequestParam String[] ids) {
        try {
            setmealService.batchRemove(ids);
            return R.success("删除成功");
        } catch (CustomException e) {
            return R.error(e.getMessage());
        }
    }

    @GetMapping("/{setmealId}")
    public R<SetmealDto> setmealSetmealId(@PathVariable String setmealId) {
        SetmealDto setmealDto = setmealService.getById(setmealId);
        return R.success(setmealDto);
    }

    @PutMapping
    @CacheEvict(value = "setMeal", allEntries = true)
    public R<String> modifySetmeal(@RequestBody SetmealDto setmealDto) {
        setmealService.modifyOne(setmealDto);
        return R.success("操作成功");
    }

    @PostMapping("/status/{status}")
    public R<String> batchModifyStatus(@PathVariable Integer status, @RequestParam("ids") String[] ids) {
        setmealService.modifyStatus(status, ids);
        return R.success("操作成功");
    }
}
