package com.itheima.controller;

import com.itheima.common.R;
import com.itheima.service.OSSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.OutputStream;

/**
 * @Author HeiMa-sw
 * @Description 文件上传和下载//TODO 17210
 * @Date 20:16 2022/6/3
 **/
@RestController
@RequestMapping("/common")
public class OSSController {
    @Autowired
    private OSSService ossService;

    @PostMapping("/upload")
    public R<String> commonUploadFile(@RequestParam MultipartFile file){
        String filename = ossService.uploadFile(file);
        return R.success(filename);
    }

    @RequestMapping("/download")
    @ResponseBody
    public R<String> commonExportFile(@RequestBody OutputStream os,@RequestParam("fileName") String objectName){
        ossService.exportFile(os,objectName);
        return R.success("OK");
    }
}
