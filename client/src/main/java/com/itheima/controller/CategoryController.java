package com.itheima.controller;

import com.itheima.common.R;
import com.itheima.entity.Category;
import com.itheima.mapper.CategoryMapper;
import com.itheima.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 分类表操作//TODO 17210
 * @Date 11:46 2022/6/10
 **/
@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/list")
    public R<List<Category>> getList() {
        List<Category> categoryList = categoryService.getList();
        return R.success(categoryList);
    }
}
