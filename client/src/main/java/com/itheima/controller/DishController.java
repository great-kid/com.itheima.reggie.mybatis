package com.itheima.controller;

import com.itheima.common.R;
import com.itheima.dto.DishDto;
import com.itheima.dto.DishFlavorDto;
import com.itheima.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 菜品信息操作//TODO 17210
 * @Date 9:54 2022/6/10
 **/
@RestController
@RequestMapping("/dish")
public class DishController {
    @Autowired
    private DishService dishService;


    @GetMapping("/list")
    public R<List<DishFlavorDto>> getList(String categoryId, Integer status) {
        List<DishFlavorDto> dishFlavorDtoList = dishService.getDishList(categoryId, status);
        return R.success(dishFlavorDtoList);
    }
}
