package com.itheima.controller;

import com.itheima.common.R;
import com.itheima.dto.DishFlavorDto;
import com.itheima.dto.SetmealDto;
import com.itheima.service.DishService;
import com.itheima.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author HeiMa-sw
 * @Description 套餐信息控制层//TODO 17210
 * @Date 13:41 2022/6/10
 **/
@RestController
@RequestMapping("/setmeal")
public class SetmealController {
    @Autowired
    private SetmealService setmealService;

    @GetMapping("/list")
    public R<List<SetmealDto>> setmealList(String categoryId, Integer status) {
        List<SetmealDto> setmealDtoList = setmealService.getList(categoryId, status);
        return R.success(setmealDtoList);
    }

    @GetMapping("/dish/{setmealId}")
    public R<List<DishFlavorDto>> setmealDishSetmealId(@PathVariable String setmealId) {
        List<DishFlavorDto> dishFlavorDtoList = setmealService.getDishFlavorsList(setmealId);
        return R.success(dishFlavorDtoList);
    }
}
