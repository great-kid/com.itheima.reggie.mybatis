package com.itheima.controller;

import com.itheima.common.AuthThreadLocal;
import com.itheima.common.R;
import com.itheima.entity.AddressBook;
import com.itheima.service.AddressBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author HeiMa-sw
 * @Description 用户地址表操作//TODO 17210
 * @Date 8:43 2022/6/10
 **/
@RestController
@RequestMapping("/addressBook")
public class AddressBookController {
    @Autowired
    private AddressBookService addressBookService;

    @GetMapping("/default")
    public R<AddressBook> addressBookDetail() {
        String userid = AuthThreadLocal.getAuth();
        AddressBook add = addressBookService.getDefault(userid);
        return R.success(add);
    }

    @PutMapping("/default")
    public R<AddressBook> addressBookDefault(@RequestBody Map<String, String> map) {
        String uid = AuthThreadLocal.getAuth();
        String id = map.get("id");
        AddressBook addressBook = addressBookService.getById(id);
        if (addressBook.getIsDefault() == 1) {
            return R.error("已经是默认地址了");
        } else {
            List<AddressBook> addressBookList = addressBookService.getList(uid);
            List<AddressBook> addressBookList1 = addressBookList.stream().map(a -> {
                a.setIsDefault(0);
                a.setUpdateTime(LocalDateTime.now());
                a.setUpdateUser(uid);
                return a;
            }).collect(Collectors.toList());
            addressBookService.updateIsDefault(addressBookList1);

            addressBook.setIsDefault(1);
            addressBook.setUpdateUser(uid);
            addressBook.setUpdateTime(LocalDateTime.now());
            addressBookService.modifyOne(addressBook);
            return R.success(addressBook);
        }
    }

    @GetMapping("/list")
    public R<List<AddressBook>> addressBookList() {
        String uid = AuthThreadLocal.getAuth();
        List<AddressBook> addressList = addressBookService.getList(uid);
        return R.success(addressList);
    }

    @GetMapping("/{id}")
    public R<AddressBook> addressBookId(@PathVariable String id) {
        AddressBook addressBook = addressBookService.getById(id);
        return R.success(addressBook);
    }

    @PostMapping
    public R<AddressBook> saveAddressBook(@RequestBody AddressBook addressBook) {
        AddressBook addressBook1 = addressBookService.saveOne(addressBook);
        return R.success(addressBook1);
    }

    @DeleteMapping
    public R<String> removeAddressBook(String ids) {
        int rows = addressBookService.removeOne(ids);
        if (rows > 0) {
            return R.success("删除成功");
        } else {
            return R.error("删除失败");
        }
    }

    @PutMapping
    public R<String> modifyAddressBook(@RequestBody AddressBook addressBook) {
        String uid = AuthThreadLocal.getAuth();
        addressBook.setUpdateUser(uid);
        addressBook.setUpdateTime(LocalDateTime.now());
        addressBookService.modifyOne(addressBook);
        return R.success("修改成功");
    }

}
