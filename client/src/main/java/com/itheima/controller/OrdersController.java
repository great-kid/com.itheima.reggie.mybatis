package com.itheima.controller;

import com.itheima.common.R;
import com.itheima.entity.Orders;
import com.itheima.mapper.ShoppingCartMapper;
import com.itheima.service.OrdersService;
import com.itheima.vo.OrdersMobileVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author HeiMa-sw
 * @Description //TODO 17210
 * @Date 20:18 2022/6/10
 **/
@RestController
@RequestMapping("/order")
public class OrdersController {
    @Autowired
    private OrdersService ordersService;
    @PostMapping("/submit")
    public R<String> orderSubmit(@RequestBody Orders orders){
        ordersService.saveOne(orders);
        return R.success("添加成功");
    }

    @GetMapping("/userPage")
    public R<OrdersMobileVo> orderUserPage(Integer page,Integer pageSize){
        OrdersMobileVo pageInfo = ordersService.getPage(page, pageSize);
        return R.success(pageInfo);
    }
}
