package com.itheima.anno;

import java.lang.annotation.*;
/**
 * @Author HeiMa-sw
 * @Description //TODO 17210
 * @Date 14:11 2022/6/13
 **/

@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IsHandleResp {
    String params() default "" ; //多个参数已,分隔
    String type() default ""; //对应多个参数已,分隔
}
