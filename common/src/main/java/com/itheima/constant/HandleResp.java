package com.itheima.constant;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.itheima.anno.IsHandleResp;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;


@ControllerAdvice
public class HandleResp implements ResponseBodyAdvice {


    @Override
    public boolean supports(MethodParameter methodParameter, Class aClass) {
        //判断一个类上是否含有该注解
        return methodParameter.hasMethodAnnotation(IsHandleResp.class);
    }

    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        String string = JSON.toJSONString(o);
        JSONObject object = JSONObject.parseObject(string);
        Class<?> classType = object.get("data").getClass();
        System.out.println("类型是>>>>>>" + classType.getSimpleName());
        IsHandleResp methodAnnotation = methodParameter.getMethodAnnotation(IsHandleResp.class);
        String[] params = methodAnnotation.params().split(",");//所有参数信息
        String[] paramTypes = methodAnnotation.type().split(",");//所有参数类型
        if (params.length != paramTypes.length) {
            return object;
        }
        //此处分开返回，因为可能返回体是Object，也可能是Array。不能篡改最终的格式
        if (classType.getSimpleName().equals("JSONObject")) {
            JSONObject result = object.getJSONObject("data");
            if (null != result) {
                for (int j = 0; j < params.length; j++) {
                    String o1 = result.getString(params[j]);
                    //如果为空值，则直接不处理
                    if (StringUtils.isEmpty(o1)) {
                        result.put(params[j], o1);
                    }
                    String type = paramTypes[j];
                    Object handleResult = handle(o1, type);
                    result.put(params[j], handleResult);
                }
                object.put("data", result);
            }
            return object;
        } else if (classType.getSimpleName().equals("JSONArray")) {
            JSONArray data = object.getJSONArray("data");
            for (int i = 0; i < data.size(); i++) {
                JSONObject jsonObject = (JSONObject) data.get(i);
                for (int j = 0; j < params.length; j++) {
                    String o1 = jsonObject.getString(params[j]);
                    //如果为空值，则直接不处理
                    if (StringUtils.isEmpty(o1)) {
                        jsonObject.put(params[j], o1);
                    }
                    String type = paramTypes[j];
                    Object handleResult = handle(o1, type);
                    jsonObject.put(params[j], handleResult);
                }
            }
            object.put("data", data);
            return object;
        } else {
            System.out.println("未知类型");
        }
        return o;
    }

    /**
     * 脱敏处理
     *
     * @param o
     * @param type
     * @return
     */
    public Object handle(Object o, String type) {
        switch (type) {
            case Constant.BANKNUMBER:
                o = bankNumberDeal(String.valueOf(o));
                break;
            case Constant.CARDID:
                o = idCardDeal(String.valueOf(o));
                break;
            case Constant.NAMES:
                o = nameDeal(String.valueOf(o));
                break;
            case Constant.PHONES:
                o = phoneDeal(String.valueOf(o));
                break;
            case Constant.FIXPHONES:
                o = fixPhoneDeal(String.valueOf(o));
                break;
            case Constant.EMAILSS:
                o = emailDeal(String.valueOf(o));
                break;
            case Constant.ADDRESS:
                o = addressDeal(String.valueOf(o));
                break;
            default:
                break;
        }
        return o;
    }

    /**
     * 处理银行卡脱敏
     *
     * @param o
     * @return
     */
    public static Object bankNumberDeal(String o) {
        int length = o.length();
        StringBuffer tempString = new StringBuffer();
        String s1 = o.substring(0, 6);
        String s2 = o.substring(6, length - 4);
        String s3 = o.substring(length - 4, length);
        tempString.append(s1);
        for (int i = 0; i < s2.length(); i++) {
            tempString.append("*");
        }
        tempString.append(s3);
        return tempString.toString();
    }

    /**
     * 处理证件脱敏
     *
     * @param o
     * @return
     */
    public static Object idCardDeal(String o) {
        int length = o.length();
        StringBuffer tempString = new StringBuffer();
        String s1;
        String s2;
        if (length > 6) {
            s1 = o.substring(0, length - 6);
            s2 = o.substring(length - 6, length);
        } else {
            return "******";
        }
        tempString.append(s1);
        for (int i = 0; i < s2.length(); i++) {
            tempString.append("*");
        }
        return tempString.toString();
    }

    /**
     * 客户姓名脱敏
     *
     * @param o
     * @return
     */
    public static Object nameDeal(String o) {
        int length = o.length();
        StringBuffer tempString = new StringBuffer();
        String s1;
        String s2;
        String s3;
        if (length >= 3) {
            s1 = o.substring(0, length - 2);
            s2 = "*";
            s3 = o.substring(length - 1, length);
        } else if (length >= 2) {
            //两位姓氏
            s1 = "*";
            s2 = "";
            s3 = o.substring(length - 1, length);
        } else {
            return "**";
        }
        tempString.append(s1);
        tempString.append(s2);
        tempString.append(s3);
        return tempString.toString();
    }

    /**
     * 手机号码脱敏
     *
     * @param o
     * @return
     */
    public static Object phoneDeal(String o) {
        int length = o.length();
        StringBuffer tempString = new StringBuffer();
        String s1 = o.substring(0, 3);
        String s2 = o.substring(3, length - 4);
        String s3 = o.substring(length - 4, length);
        tempString.append(s1);
        for (int i = 0; i < s2.length(); i++) {
            tempString.append("*");
        }
        tempString.append(s3);
        return tempString.toString();
    }

    /**
     * 固定电话脱敏
     *
     * @param o
     * @return
     */
    public static Object fixPhoneDeal(String o) {
        int length = o.length();
        StringBuffer tempString = new StringBuffer();
        String s1 = o.substring(0, 4);
        String s2 = o.substring(4, length - 2);
        String s3 = o.substring(length - 2, length);
        tempString.append(s1);
        for (int i = 0; i < s2.length(); i++) {
            tempString.append("*");
        }
        tempString.append(s3);
        return tempString.toString();
    }

    /**
     * 电子邮箱脱敏
     *
     * @param o
     * @return
     */
    public static Object emailDeal(String o) {
        StringBuffer tempString = new StringBuffer();
        String tag = "@";
        String[] split = o.split(tag);
        String s1 = split[0];
        String s2 = split[1];
        String s11;
        if (s1.length() > 3) {
            s11 = s1.substring(0, 3);
        } else {
            s11 = s1;
        }
        String s12 = "***";
        tempString.append(s11);
        tempString.append(s12);
        tempString.append(tag);
        tempString.append(s2);
        return tempString.toString();
    }

    /**
     * 地址脱敏
     *
     * @param o
     * @return
     */
    public static Object addressDeal(String o) {
        StringBuffer tempString = new StringBuffer();
        int leng = o.length();
        String s1;
        String s2;
        if (leng > 6) {
            s1 = o.substring(0, leng - 6);
            s2 = "******";
        } else {
            s1 = o;
            s2 = "";
        }
        tempString.append(s1);
        tempString.append(s2);
        return tempString.toString();
    }
}


