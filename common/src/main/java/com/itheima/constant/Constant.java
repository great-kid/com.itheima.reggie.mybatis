package com.itheima.constant;

public class Constant {
    public static final String BANKNUMBER="1";//卡号脱敏
    public static final String CARDID="2";//身份证脱敏
    public static final String NAMES="3";//姓名脱敏
    public static final String PHONES="4";//手机号码脱敏
    public static final String FIXPHONES="5";//固定号码脱敏
    public static final String EMAILSS="6";//邮箱脱敏
    public static final String ADDRESS="7";//地址信息脱敏
}