package com.itheima.common;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author 黑马程序员-孙文
 * @Description 全局异常处理//TODO 孙文
 * @Date 2022/6/26 8:41
 **/
@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(CustomException.class)
    @ResponseBody
    public void businessExceptionHandler(CustomException ex){
        System.out.println("customException异常被捕获了");
        ex.printStackTrace();
    }
    public void exception(Exception e){
        e.printStackTrace();
    }
}
