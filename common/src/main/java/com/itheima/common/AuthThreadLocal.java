package com.itheima.common;

/**
 * @Author 黑马程序员-孙文
 * @Description //TODO 孙文
 * @Date 2022/6/25 16:06
 **/
public class AuthThreadLocal {
    public static ThreadLocal<String> auth = new ThreadLocal<>();

    public static String getAuth() {
        return auth.get();
    }

    public static void setAuth(String value) {
        auth.set(value);
    }

    public static void remove() {
        auth.remove();
    }
}
